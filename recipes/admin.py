from django.contrib import admin
from recipes.models import Recipe, RecipeStep, Ingredient


# Register your models here.
# admin.site.register(Recipe)


# fancy way to register model
@admin.register(Recipe)
class RecipeAdmin(admin.ModelAdmin):
    list_display = ["title", "id"]


# you can use this to change how it appears in the admin page
@admin.register(RecipeStep)
class RecipeStepAdmin(admin.ModelAdmin):
    list_display = ("step_number", "instruction", "id", "recipe")


@admin.register(Ingredient)
class RecipeIngreientAdmin(admin.ModelAdmin):
    list_display = ("amount", "food_item", "id", "recipe")
