from django.shortcuts import render, get_object_or_404, redirect
from recipes.models import Recipe
from recipes.forms import RecipeForm
from django.contrib.auth.decorators import login_required


# Create your views here.
def show_recipe(request, id: int):
    recipe = get_object_or_404(Recipe, id=id)
    return render(request, "recipes/detail.html", {"recipe": recipe})


def list_recipes(request):
    # recipe_list = get_list_or_404(Recipe)
    recipe_list = Recipe.objects.all()
    return render(request, "recipes/list.html", {"recipe_list": recipe_list})


@login_required
def my_recipe_list(request):
    recipes = Recipe.objects.filter(author=request.user)
    return render(request, "recipes/list.html", {"recipe_list": recipes})


@login_required
def create_recipe(request):
    if request.method == "GET":
        form = RecipeForm()
    elif request.method == "POST":
        form = RecipeForm(request.POST)
        if form.is_valid():
            recipe = form.save(False)
            recipe.author = request.user
            recipe.save()
            return redirect("list_recipes")
    return render(request, "recipes/create.html", {"form": form})


def edit_recipe(request, id: int):
    post = get_object_or_404(Recipe, id=id)
    if request.method == "POST":
        form = RecipeForm(request.POST, instance=post)
        if form.is_valid():
            form.save()
            return redirect("show_recipe", id=id)
    else:
        form = RecipeForm(instance=post)
    return render(request, "recipes/edit.html", {"recipe": post, "recipe_form": form})
